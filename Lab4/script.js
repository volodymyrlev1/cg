const canvas = document.getElementById('canv');
const ctx = canvas.getContext('2d');
const rgbColorElement = document.getElementById('rgbcolor');
const hsvColorElement = document.getElementById('hsvcolor');
const cmykColorElement = document.getElementById('cmykcolor');

const hueInput = document.getElementById('hue');
const saturationInput = document.getElementById('saturation');
const valueInput = document.getElementById('value');
const cyanInput = document.getElementById('cyan');
const magentaInput = document.getElementById('magenta');
const yellowInput = document.getElementById('yellow');
const blackInput = document.getElementById('black');
const applyHSVButton = document.getElementById('applyHSV');
const applyCMYKButton = document.getElementById('applyCMYK');
const valueSlider = document.getElementById("valueSlider");
const saveChanges = document.getElementById('saveChanges');
const allColors = document.getElementById("allColors");

let imageData;
let originalImageData;
let revertData;

console.log(rgbToHsv(255, 0, 0)); // should output [0, 1, 1]
console.log(hsvToRgb(0, 1, 1)); // should output [255, 0, 0]
console.log(rgbToCmyk(255, 0, 0)); // should output [0, 1, 1, 0]
console.log(cmykToRgb(0, 1, 1, 0));


let selectedPixelX, selectedPixelY;

let isDragging = false;
let startX, startY, endX, endY;

canvas.addEventListener('mousedown', (e) => {
    if (e.button === 2) {
        isDragging = true;
        startX = e.offsetX;
        startY = e.offsetY;
    }
});

canvas.addEventListener('mousemove', (e) => {
    if (isDragging) {
        endX = e.offsetX;
        endY = e.offsetY;
    }
});

canvas.addEventListener('mouseup', (e) => {
    if (e.button === 2) {
        const selectection = document.getElementById("selection").checked;
        isDragging = false;

        if(selectection) 
        {
            drawSelectionRect();
        }
    }


    const data = originalImageData.data;
    const pinkRGB = [255, 192, 203]; 
    let delta = 20;
    let found = false;
    for (let y = 0; y < originalImageData.height; y++) {
        for (let x = 0; x < originalImageData.width; x++) {
            const index = (y * originalImageData.width + x) * 4;
            const r = data[index];
            const g = data[index + 1];
            const b = data[index + 2];

            if (x >= startX && x <= endX && y >= startY && y <= endY) {
                if (Math.abs(r - pinkRGB[0]) <= delta && Math.abs(g - pinkRGB[1]) <= delta && Math.abs(b - pinkRGB[2]) <= delta) {
                    let [h,s,v] = rgbToHsv(r,g,b);
                    found = true;
                    valueSlider.value = v*100;
                    console.log("SAVED: ",v);
                    break;
                }
            } 
        }

        if(found) break;
    }

});

canvas.addEventListener('contextmenu', (e) => {
    e.preventDefault();
});

function drawSelectionRect() {

    ctx.strokeStyle = 'rgba(255, 255, 255, 0.8)';
    ctx.lineWidth = 2;
    ctx.strokeRect(startX, startY, endX - startX, endY - startY);
}

canvas.addEventListener('click', function(event) {
    const x = event.offsetX;
    const yC = event.offsetY;
    
    selectedPixelX = x;
    selectedPixelY = yC;

    const imageData = ctx.getImageData(x, yC, 1, 1);
    const data = imageData.data;
    const [r, g, b] = [data[0], data[1], data[2]];

    const [h, s, v] = rgbToHsv(r, g, b);
    const [c, m, y, k] = rgbToCmyk(r, g, b);

    hueInput.value = Math.round(h * 360);
    saturationInput.value = Math.round(s * 100);
    valueInput.value = Math.round(v * 100);

    cyanInput.value = Math.round(c * 100);
    magentaInput.value = Math.round(m * 100);
    yellowInput.value = Math.round(y * 100);
    blackInput.value = Math.round(k * 100);

    rgbColorElement.textContent = `(${r}, ${g}, ${b})`;
    hsvColorElement.textContent = `(${h.toFixed(2)}, ${s.toFixed(2)}, ${v.toFixed(2)})`;
    cmykColorElement.textContent = `(${c.toFixed(2)}, ${m.toFixed(2)}, ${y.toFixed(2)}, ${k.toFixed(2)})`;
});

[hueInput, saturationInput, valueInput, cyanInput, magentaInput, yellowInput, blackInput].forEach(input => {
    input.addEventListener('input', updateColors);
});

applyHSVButton.addEventListener('click', function(){
    if(allColors.checked){
        applyImageHsv();
    } else {
        applyChangesToHSV();
    }
});

applyCMYKButton.addEventListener('click', function(){
    if(allColors.checked){
        applyImageCmyk();
    } else {
        applyChangesToCMYK();
    }
});


$("#load").click(function(){
    var input = document.createElement('input');
    input.type = 'file';
    
    input.onchange = function() {
        var file = input.files[0];
        var reader = new FileReader();

        reader.onload = function(event) {
            var img = new Image();
            img.onload = function() {
                canvas.width = img.width;
                canvas.height = img.height;

                if(img.width > 1300){
                    canvas.width = 1300;
                }
                if(img.height > 900){
                    canvas.height = 900;
                }

                ctx.drawImage(img, 0, 0);
                imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

                originalImageData = imageData;
                revertData = imageData;
                updateColors();
            };
            img.src = event.target.result;
        };
        
        reader.readAsDataURL(file);
    };

    input.click();
})

$("#save").click(function(){
    let canvUrl = canvas.toDataURL();
    const blob = dataURLtoBlob(canvUrl);
    const url = URL.createObjectURL(blob);
    
    const createEl = document.createElement('a');
    
    createEl.href = url;
    createEl.download = 'my_canvas_image.png';

    createEl.click();

    createEl.remove();

    URL.revokeObjectURL(url);
});

function dataURLtoBlob(dataURL) {
    const parts = dataURL.split(';base64,');
    const contentType = parts[0].split(':')[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}

$("#hsv").click(function(){
    imageData = ctx.createImageData(imageData.width, imageData.height);
    convertImageToHSV(imageData,0,0,0);
})

$("#cmyk").click(function(){
    imageData = ctx.createImageData(imageData.width, imageData.height);
    convertImageToCmyk(imageData,0,0,0,0);
})

// colors

function hsvToRgb(h, s, v) {
    let r, g, b;
    const i = Math.floor(h * 6);
    const f = h * 6 - i;
    const p = v * (1 - s);
    const q = v * (1 - f * s);
    const t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return [r * 255, g * 255, b * 255];
}
function rgbToCmyk(r, g, b) {
    let c, m, y, k;
    c = 1 - (r / 255);
    m = 1 - (g / 255);
    y = 1 - (b / 255);
    k = Math.min(c, Math.min(m, y));
    c = (c - k) / (1 - k);
    m = (m - k) / (1 - k);
    y = (y - k) / (1 - k);
    return [c, m, y, k];
}

function cmykToRgb(c, m, y, k) {
    const r = 255 * (1 - c) * (1 - k);
    const g = 255 * (1 - m) * (1 - k);
    const b = 255 * (1 - y) * (1 - k);
    return [r, g, b];
}

function rgbToHsv(r, g, b) {
    r /= 255, g /= 255, b /= 255;

    const max = Math.max(r, g, b), min = Math.min(r, g, b);
    let h, s, v = max;

    const d = max - min;
    s = max === 0 ? 0 : d / max;

    if (max === min) {
        h = 0; // achromatic
    } else {
        switch (max) {
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [h, s, v];
}

function hsvToCmyk(h, s, v) {
    const [r, g, b] = hsvToRgb(h, s, v);
    return rgbToCmyk(r * 255, g * 255, b * 255);
}

function cmykToHsv(c, m, y, k) {
    const [r, g, b] = cmykToRgb(c, m, y, k);
    return rgbToHsv(r, g, b);
}

function updateColors() {
    const [r, g, b] = [imageData.data[0], imageData.data[1], imageData.data[2]];
    const [h, s, v] = rgbToHsv(r, g, b);
    const [c, m, y, k] = rgbToCmyk(r, g, b);

    rgbColorElement.textContent = `(${r}, ${g}, ${b})`;
    hsvColorElement.textContent = `(${h.toFixed(2)}, ${s.toFixed(2)}, ${v.toFixed(2)})`;
    cmykColorElement.textContent = `(${c.toFixed(2)}, ${m.toFixed(2)}, ${y.toFixed(2)}, ${k.toFixed(2)})`;
}

function applyChangesToHSV() {
    const hue = parseInt(hueInput.value);
    const saturation = parseInt(saturationInput.value);
    const value = parseInt(valueInput.value);

    applyChanges(imageData, hue, saturation, value);
}

function applyChangesToCMYK() {
    const cyan = parseInt(cyanInput.value);
    const magenta = parseInt(magentaInput.value);
    const yellow = parseInt(yellowInput.value);
    const black = parseInt(blackInput.value);

    applyChanges(imageData, cyan, magenta, yellow, black);
}

function applyChanges(imageData, ...args) {
    const data = originalImageData.data;
    const newImageData = ctx.createImageData(originalImageData.width, originalImageData.height);

    const newData = newImageData.data;
    const delta = 20; 

    if (selectedPixelX !== undefined && selectedPixelY !== undefined) {
        const selectedPixelIndex = (selectedPixelY * originalImageData.width + selectedPixelX) * 4;
        const selectedR = data[selectedPixelIndex];
        const selectedG = data[selectedPixelIndex + 1];
        const selectedB = data[selectedPixelIndex + 2];

        for (let i = 0; i < data.length; i += 4) {
            const r = data[i];
            const g = data[i + 1];
            const b = data[i + 2];

            // Check if the current pixel color is within the delta range of the selected color
            if (Math.abs(r - selectedR) <= delta && Math.abs(g - selectedG) <= delta && Math.abs(b - selectedB) <= delta) {
                let newColor;

                if (args.length === 3) {
                    const [h, s, v] = rgbToHsv(r, g, b);
                    const [newH, newS, newV] = applyHSVChanges(h, s, v, ...args);
                    newColor = hsvToRgb(newH, newS, newV);
                } else {
                    const [c, m, y, k] = rgbToCmyk(r, g, b);
                    const [newC, newM, newY, newK] = applyCMYKChanges(c, m, y, k, ...args);
                    newColor = cmykToRgb(newC, newM, newY, newK);
                }

                newData[i] = newColor[0];
                newData[i + 1] = newColor[1];
                newData[i + 2] = newColor[2];
                newData[i + 3] = data[i + 3];
            } else {
                newData[i] = data[i];
                newData[i + 1] = data[i + 1];
                newData[i + 2] = data[i + 2];
                newData[i + 3] = data[i + 3];
            }
        }
    }
    
    if(saveChanges.checked)
    {
        originalImageData = newImageData;
    }

    ctx.putImageData(newImageData, 0, 0);
    
    updateColors();
}

function changePink(valueDelta) {
    if (startX !== undefined && startY !== undefined && endX !== undefined && endY !== undefined) {
        const data = originalImageData.data;
        const newImageData = ctx.createImageData(originalImageData.width, originalImageData.height);
        const newData = newImageData.data;
        const delta = 100; 

        const pinkRGB = [255, 192, 203]; 

        for (let y = 0; y < originalImageData.height; y++) {
            for (let x = 0; x < originalImageData.width; x++) {
                const index = (y * originalImageData.width + x) * 4;
                const r = data[index];
                const g = data[index + 1];
                const b = data[index + 2];

                if (x >= startX && x <= endX && y >= startY && y <= endY) {
                    if (Math.abs(r - pinkRGB[0]) <= delta && Math.abs(g - pinkRGB[1]) <= delta && Math.abs(b - pinkRGB[2]) <= delta) {
                        let [h, s, v] = rgbToHsv(r, g, b);

                        v = Math.min(1, valueDelta / 100);

                        const [newR, newG, newB] = hsvToRgb(h, s, v);

                        newData[index] = newR;
                        newData[index + 1] = newG;
                        newData[index + 2] = newB;
                        newData[index + 3] = data[index + 3]; 
                    } else {
                        newData[index] = r;
                        newData[index + 1] = g;
                        newData[index + 2] = b;
                        newData[index + 3] = data[index + 3];
                    }
                } else {
                    newData[index] = r;
                    newData[index + 1] = g;
                    newData[index + 2] = b;
                    newData[index + 3] = data[index + 3];
                }
            }
        }


        if(saveChanges.checked)
        {
            originalImageData = newImageData;
        }
    
        ctx.putImageData(newImageData, 0, 0);

        updateColors();
    }
}



function applyHSVChanges(h, s, v, hueDelta, saturationDelta, valueDelta) {
    const newHue = (hueDelta / 360) % 1; 
    const newSaturation = Math.min(1,saturationDelta / 100);
    const newValue = Math.min(1, valueDelta / 100); 

    return [newHue, newSaturation, newValue];
}

function applyCMYKChanges(c, m, y, k, cyanDelta, magentaDelta, yellowDelta, blackDelta) {
    const scaleFactor = 0.01; 
    const newCyan = clamp(c + cyanDelta * scaleFactor); 
    const newMagenta = clamp(m + magentaDelta * scaleFactor);
    const newYellow = clamp(y + yellowDelta * scaleFactor); 
    const newBlack = clamp(k + blackDelta * scaleFactor); 
    
    return [cyanDelta*scaleFactor, magentaDelta*scaleFactor, yellowDelta*scaleFactor, blackDelta*scaleFactor];
}


function clamp(value) {
    return Math.max(0, Math.min(1, value));
}

function convertImageToHSV(imageData, hue, saturation, value) {
    const newImageData = ctx.createImageData(imageData.width, imageData.height);
    const data = imageData.data;
    const newData = newImageData.data;

    for (let i = 0; i < data.length; i += 4) {
        const r = data[i];
        const g = data[i + 1];
        const b = data[i + 2];
        const [h, s, v] = rgbToHsv(r, g, b);

        // Modify HSV values based on sliders
        const newH = (hue / 360) % 1; // Hue value is normalized to [0, 1]
        const newS = Math.min(1, saturation / 100); // Saturation value is normalized to [0, 1]
        const newV = Math.min(1, value / 100); // Value value is normalized to [0, 1]

        // Convert modified HSV values back to RGB
        const [newR, newG, newB] = hsvToRgb(newH, newS, newV);

        // Set the new RGB values in the new image data
        newData[i] = newR;
        newData[i + 1] = newG;
        newData[i + 2] = newB;
        newData[i + 3] = data[i + 3]; // Alpha channel remains the same
    }

    return newImageData;
}

function convertImageToCMYK(imageData, cyan, magenta, yellow, black) {
    const newImageData = ctx.createImageData(imageData.width, imageData.height);
    const data = imageData.data;
    const newData = newImageData.data;

    for (let i = 0; i < data.length; i += 4) {
        const r = data[i];
        const g = data[i + 1];
        const b = data[i + 2];
        const [c, m, y, k] = rgbToCmyk(r, g, b);

        const newC = Math.min(100, c + cyan); 
        const newM = Math.min(100, m + magenta); 
        const newY = Math.min(100, y + yellow); 
        const newK = Math.min(100, k + black); 

        const [newR, newG, newB] = cmykToRgb(newC, newM, newY, newK);

        newData[i] = newR;
        newData[i + 1] = newG;
        newData[i + 2] = newB;
        newData[i + 3] = data[i + 3]; 
    }

    return newImageData;
}

$("#applyPinkButton").click(function(){
    let newValue = parseInt(document.getElementById('valueSlider').value);
    
    changePink(newValue);
})


// CMYK change to all
function applyImageHsv() {
    const hue = parseInt(hueInput.value);
    const saturation = parseInt(saturationInput.value);
    const value = parseInt(valueInput.value);

    applyChangesWholeImage(imageData, hue, saturation, value);
}

function applyImageCmyk() {
    const cyan = parseInt(cyanInput.value);
    const magenta = parseInt(magentaInput.value);
    const yellow = parseInt(yellowInput.value);
    const black = parseInt(blackInput.value);

    applyChangesWholeImage(imageData, cyan, magenta, yellow, black);
}

function applyChangesWholeImage(imageData, ...args) {
    const newImageData = ctx.createImageData(imageData.width, imageData.height);
    const data = imageData.data;
    const newData = newImageData.data;

    for (let i = 0; i < data.length; i += 4) {
        const r = data[i];
        const g = data[i + 1];
        const b = data[i + 2];
        let newColor;

        if (args.length === 3) {
            const [h, s, v] = rgbToHsv(r, g, b);
            const [newH, newS, newV] = applyHSVChangesWhole(h, s, v, ...args);
            newColor = hsvToRgb(newH, newS, newV);
        } else {
            const [c, m, y, k] = rgbToCmyk(r, g, b);
            const [newC, newM, newY, newK] = applyCMYKChangesWhole(c, m, y, k, ...args);
            newColor = cmykToRgb(newC, newM, newY, newK);
        }

        newData[i] = newColor[0];
        newData[i + 1] = newColor[1];
        newData[i + 2] = newColor[2];
        newData[i + 3] = data[i + 3]; 
    }

    ctx.putImageData(newImageData, 0, 0);

    updateColors();
}

function applyHSVChangesWhole(h, s, v, hueDelta, saturationDelta, valueDelta) {
    const newHue = (h + hueDelta / 360) % 1; 
    const newSaturation = Math.min(1, s + saturationDelta / 100);
    const newValue = Math.min(1, v + valueDelta / 100);
    return [newHue, newSaturation, newValue];
}

function applyCMYKChangesWhole(c, m, y, k, cyanDelta, magentaDelta, yellowDelta, blackDelta) {
    const scaleFactor = 0.01; 
    const newCyan = clamp(c + cyanDelta * scaleFactor); 
    const newMagenta = clamp(m + magentaDelta * scaleFactor);
    const newYellow = clamp(y + yellowDelta * scaleFactor); 
    const newBlack = clamp(k + blackDelta * scaleFactor); 
    return [newCyan, newMagenta, newYellow, newBlack];
}