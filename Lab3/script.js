const canvas = document.getElementById('canv');
const ctx = canvas.getContext('2d');

let cx;
let cy;
let radius = 4;
let colorScheme;
let doesBelongToAttr;
let maxIter;
let startX = 0;
let startY = 0;
let step = 1;
let pixelSize = 1;
let zoomFactor;

let mutation;
let rr;
let ll;
let ms;

let choice;


document.getElementById('mutation').addEventListener('input', function(){
    mutation = +document.getElementById('mutation').value;
    if(mutation === 0) return;

    if(choice === "fern")
        drawBarnsleyFern();
})

document.getElementById('ll').addEventListener('input', function(){
    ll = +document.getElementById('ll').value;

    if(mutation === 0) return;
    if(choice === "fern")
        drawBarnsleyFern();
})

document.getElementById('rr').addEventListener('input', function(){
    rr = +document.getElementById('rr').value;

    if(mutation === 0) return;
    if(choice === "fern")
        drawBarnsleyFern();
})

document.getElementById('ms').addEventListener('input', function(){
    ms = +document.getElementById('ms').value;

    if(mutation === 0) return;
    if(choice === "fern")
        drawBarnsleyFern();
})


document.getElementById('save').addEventListener('click', function(){
    let canvUrl = canvas.toDataURL();
    const blob = dataURLtoBlob(canvUrl);
    const url = URL.createObjectURL(blob);
    
    const createEl = document.createElement('a');
    
    createEl.href = url;
    createEl.download = 'my_canvas_image.png';

    createEl.click();

    createEl.remove();

    URL.revokeObjectURL(url);
});

function dataURLtoBlob(dataURL) {
    const parts = dataURL.split(';base64,');
    const contentType = parts[0].split(':')[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}

document.getElementById('draw').addEventListener('click', function () {

    canvas.width = 1000;
    canvas.height = 1000;

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    cx = +document.getElementById('cx').value;
    cy = +document.getElementById('cy').value;
    colorScheme = document.getElementById('colorscheme').value;
    doesBelongToAttr = document.getElementById('check-attr').checked
    maxIter = +document.getElementById('input-iter').value;
    step = +document.getElementById('input-step').value;
    mutation = +document.getElementById('mutation').value;
    zoomFactor = (1-document.getElementById('zoom').value);
    radius = +document.getElementById('radius').value;
    rr = +document.getElementById('rr').value;
    ll = +document.getElementById('ll').value;
    ms = +document.getElementById('ms').value;

    if(radius === 0) radius = 4;

    console.log(cx,cy,colorScheme,doesBelongToAttr,maxIter,startX,startY,step);

    pixelSize = 1;
    
    choice = document.querySelector("#select-fractal").value;

    if(choice === "julia") {
        drawJuliaFractal(cx,cy);

    }
    else if(choice === "fern") {
        drawBarnsleyFern();
    }
    
});

// 0.4 0.002275
function drawJuliaFractal(cx, cy) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    let initialHue = Math.random() * 360;

    for (let x = 0; x < canvas.width; x++) {
        for (let y = 0; y < canvas.height; y++) {
            let zx = ((x - canvas.width / 2) / (canvas.width / 4 * pixelSize) + startX)*zoomFactor;
            let zy = ((y - canvas.height / 2) / (canvas.height / 4 * pixelSize) + startY)*zoomFactor;
            let i = 0;

            while (i < maxIter) {
                let xtemp = zx * zx * zx - 3 * zx * zy * zy + cx;
                let ytemp = 3 * zx * zx * zy - zy * zy * zy + cy;

                if (zx * zx + zy * zy > radius) {
                    if(doesBelongToAttr)
                        break;
                    else{
                        let color;
                        if(colorScheme === 'grayscale') {
                            color = "black";
                        } else if (colorScheme === 'rainbow') {
                            let rgb = hslToRgb(1 / 360, 1, 0.5);
                            color = `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`;
                        } else if (colorScheme === 'random') {
                            let rgb = hslToRgb(initialHue/100*i/ maxIter, 1, 0.5);
                            color = `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`;
                        }
            
                        ctx.fillStyle = color;
                        ctx.fillRect(x, y, 1, 1);
                        break;
                    }
                }

                zx = xtemp;
                zy = ytemp;
                i++;
            }

            let color;
            if(doesBelongToAttr) {
                if(colorScheme === 'grayscale') {
                    let brightness = Math.floor((i / maxIter) * 255);
                    color = `rgb(${brightness},${brightness},${brightness})`;
                } else if (colorScheme === 'rainbow') {
                    let hue = (i / maxIter) * 360;
                    let rgb = hslToRgb(hue / 360, 1, 0.5);
                    color = `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`;
                } else if (colorScheme === 'random') {
                    let rgb = hslToRgb(initialHue/100*i / maxIter, 1, 0.5);
                    color = `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`;
                }
    
                ctx.fillStyle = color;
                ctx.fillRect(x, y, 1, 1);
            }
        }
    }
}



function hslToRgb(h, s, l){
    let r, g, b;

    if(s == 0){
        r = g = b = l;
    }else{
        const hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        };

        const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        const p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

function drawBarnsleyFern() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    let x = startX;
    let y = startY;

    console.log("startX:", startX);
    console.log("startY:", startY);

    if(mutation === 0) mutation = 1;
    if(step === 0) step = 1;

    for (let i = 0; i < maxIter; i+=step) {
        let nextX, nextY;
        let r = Math.random();

        if (r < 0.01) {
            nextX = 0;
            nextY = 0.16 * y;
        } else if (r < 0.86) {
            nextX = 0.85 * x + mutation * y;
            nextY = -0.04 * x + 0.85 * y + 1.6;
        } else if (r < 0.93) {
            nextX = 0.2 * x - 0.26 * y + ll;
            nextY = 0.23 * x + 0.22 * y + 1.6;
        } else {
            nextX = -0.15 * x + 0.28 * y + rr;
            nextY = 0.26 * x + 0.24 * y + 0.44;
        }

        // x = nextX * mutation;
        // y = nextY * mutation;

        x = nextX * ms;
        y = nextY * ms;

        let color;
        if (colorScheme === 'grayscale') {
            let intensity = Math.floor((maxIter - i) / maxIter * 255);
            color = `rgb(${intensity}, ${intensity}, ${intensity})`;
        } else if (colorScheme === 'rainbow') {
            let hue = (i / maxIter) * 360;
            let rgb = hslToRgb(hue / 360, 1, 0.5);
            color = `rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`;
        } else if (colorScheme === 'random') {
            let intensity = Math.floor((i / maxIter) * 255);
            color = `rgb(0, ${intensity}, 0)`; 
        }

        let screenX = Math.floor(canvas.width / 2 + x * 50);
        let screenY = Math.floor(canvas.height - y * 50);

        ctx.fillStyle = color;
        ctx.fillRect(screenX, screenY, 1, 1);
    }
}