const canvas = document.getElementById('canv');
const ctx = canvas.getContext('2d');

let origGridSizeX, origGridSizeY;
let gridSizeX;
let gridSizeY;
let gridSpacing = 50;

$( document ).ready(() => {
    canvas.width = 1000;
    canvas.height = 1000;
});

let rect;
let scaleMatrix;

class Rectangle {
    constructor(x1, y1, width, height) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x1 + width;
        this.y2 = y1 - height;
        console.log(this.x1, this.y1, this.x2, this.y2);
    }

    draw(ctx) {
        ctx.fillStyle = $("#rectColor").val();
    
        const x1Pixels = (this.x1 + canvas.width / 2 / gridSpacing) * gridSpacing;
        const y1Pixels = (canvas.height / 2 / gridSpacing - this.y1) * gridSpacing;
        const x2Pixels = (this.x2 + canvas.width / 2 / gridSpacing) * gridSpacing;
        const y2Pixels = (canvas.height / 2 / gridSpacing - this.y2) * gridSpacing;
    
        ctx.beginPath();
        ctx.fillRect(x1Pixels, y2Pixels, (x2Pixels - x1Pixels), (y1Pixels - y2Pixels));
        ctx.stroke();
    }

    transform(matrix, coefficient) {
        const scaleMatrix = scalingMatrix(coefficient, coefficient);
        const combinedMatrix = multiplyMatrices(matrix, scaleMatrix);
        const originalPoints = [[this.x1, this.y1], [this.x2, this.y2]];
        const transformedPoints = [];
        for (let i = 0; i < originalPoints.length; i++) {
            const point = originalPoints[i];
            const x = point[0];
            const y = point[1];
            const w = 1;
            const newX = combinedMatrix[0][0] * x + combinedMatrix[0][1] * y + combinedMatrix[0][2] * w;
            const newY = combinedMatrix[1][0] * x + combinedMatrix[1][1] * y + combinedMatrix[1][2] * w;
            transformedPoints.push([newX, newY]);
        }
        return transformedPoints;
    }

    updatePoints(points) {
        this.x1 = points[0][0];
        this.y1 = points[0][1];
        this.x2 = points[1][0];
        this.y2 = points[1][1];
        console.log(this.x1, this.y1, this.x2, this.y2);
    }
}


$("#saveScale").click(function(){
    gridSpacing = parseInt($("#scale").val());
    $("#drawGrid").click();
})

$("#createRectangle").click(function(){
    const width = Math.abs(+$("#bottomRightX").val() - +$("#topLeftX").val());
    const height = Math.abs(+$("#bottomRightY").val() - +$("#topLeftY").val());
    rect = new Rectangle(+$("#topLeftX").val(), +$("#topLeftY").val(), width, height);
    
    console.log(+$("#topLeftX").val(), +$("#topLeftY").val(), width, height);

    rect.draw(ctx);
});


function drawGrid(){
    const _gridSizeX = parseInt($("#gridSizeX").val());
    const _gridSizeY = parseInt($("#gridSizeY").val());
    origGridSizeX = _gridSizeX;
    origGridSizeY = _gridSizeY;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;

    gridSizeX = _gridSizeX; 
    gridSizeY = _gridSizeY; 

    // Розрахунок кількості ліній сітки на основі розміру canvas та gridSpacing
    const numGridLinesX = Math.ceil(canvas.width / gridSpacing);
    const numGridLinesY = Math.ceil(canvas.height / gridSpacing);

    // Відрисовка вертикальних ліній сітки
    for (let i = 0; i <= numGridLinesX; i++) {
        const x = i * gridSpacing;
        ctx.beginPath();
        ctx.moveTo(x, 0);
        ctx.lineTo(x, canvas.height);
        ctx.stroke();
    }

    // Відрисовка горизонтальних ліній сітки
    for (let i = 0; i <= numGridLinesY; i++) {
        const y = i * gridSpacing;
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(canvas.width, y);
        ctx.stroke();
    }

    ctx.strokeStyle = "blue";
    ctx.lineWidth = 2;

    // Draw X and Y axes
    ctx.beginPath();
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width / 2, canvas.height);
    ctx.stroke();

    // Draw axis labels
    ctx.fillStyle = "blue";
    ctx.font = "12px Arial";
    ctx.textAlign = "center";
    for (let i = 0; i <= numGridLinesX; i++) {
        const x = i * gridSizeX;
        if (x === canvas.width / 2) {
            continue;
        }
        const origX = (x - canvas.width / 2) / gridSpacing;
        ctx.fillText(origX, x, canvas.height / 2 + 15);
    }
    ctx.fillText("X", canvas.width - 10, canvas.height / 2 + 15);

    for (let i = 0; i <= numGridLinesY; i++) {
        const y = i * gridSizeY;
        if (y === canvas.height / 2) {
            continue;
        }
        const origY = -(y - canvas.height / 2) / gridSpacing;
        ctx.fillText(origY, canvas.width / 2 - 10, y + 5);
    }
    ctx.fillText("Y", canvas.width / 2 - 10, 10);

    // Draw arrowheads
    drawArrowhead(ctx, canvas.width, canvas.height / 2, Math.PI / 2); 
    drawArrowhead(ctx, canvas.width / 2, 0, 0);
}

$("#drawGrid").click(drawGrid);

function drawArrowhead(ctx, x, y, angle) {
    ctx.save();
    ctx.beginPath();
    ctx.translate(x, y);
    ctx.rotate(angle);
    ctx.moveTo(0, 0);
    ctx.lineTo(5, 10);
    ctx.lineTo(-5, 10);
    ctx.closePath();
    ctx.fill();
    ctx.restore();
}

let intervalId;

let isPressed = 0;

let movementMatrices = [];
let scaleMatrices = [];
let transformedPointsMatrices = [];

$("#playButton").click(function() {
    if (isPressed === 1) {
        return;
    }
    isPressed = 1;
    if ($('#saveScreenshot').is(':checked')) {
        saveImg();
    }
    const time = parseFloat($("#time").val());
    const speed = parseFloat($("#speed").val());
    let coefficient = parseFloat($("#coefficient").val());
    const deltaX = rect.x2 - rect.x1;
    const deltaY = rect.y2 - rect.y1;
    const totalDistance = Math.sqrt(deltaX ** 2 + deltaY ** 2);
    const totalTime = totalDistance / speed;
    const angle = Math.atan2(deltaY, deltaX);
    const stepDistance = totalDistance / totalTime;
    let currentTime = 0;
    intervalId = setInterval(function() {
        drawGrid();
        const translation = $("#opposite").is(":checked") ? -stepDistance : stepDistance;
        const translationMatrix = translationMatrixF(translation * Math.cos(angle), translation * Math.sin(angle));
        const scaledPoints = rect.transform(translationMatrix, coefficient);
        rect.updatePoints(scaledPoints);
        rect.draw(ctx);

        if ($('#saveMatrix').is(':checked')) {
            movementMatrices.push(translationMatrix);
            scaleMatrices.push(scalingMatrix(coefficient, coefficient));
            transformedPointsMatrices.push(scaledPoints);
        }

        currentTime += 1;
    }, time);
});

$("#stopButton").click(function() {
    isPressed = 0;
    clearInterval(intervalId);
    if ($('#saveMatrix').is(':checked')) {
        downloadMatricesFile();
    }
});

function saveImg(){
    let canvUrl = canvas.toDataURL();
    const blob = dataURLtoBlob(canvUrl);
    const url = URL.createObjectURL(blob);
    
    const createEl = document.createElement('a');
    
    createEl.href = url;
    createEl.download = 'my_canvas_image.png';

    createEl.click();

    createEl.remove();

    URL.revokeObjectURL(url);
}

function dataURLtoBlob(dataURL) {
    const parts = dataURL.split(';base64,');
    const contentType = parts[0].split(':')[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}

function translationMatrixF(tx, ty) {
    return [
        [1, 0, tx],
        [0, 1, ty],
        [0, 0, 1]
    ];
}

function scalingMatrix(sx, sy) {
    return [
        [sx, 0, 0],
        [0, sy, 0],
        [0, 0, 1]
    ];
}

function multiplyMatrices(a, b) {
    const result = [];
    for (let i = 0; i < a.length; i++) {
        result[i] = [];
        for (let j = 0; j < b[0].length; j++) {
            let sum = 0;
            for (let k = 0; k < a[0].length; k++) {
                sum += a[i][k] * b[k][j];
            }
            result[i][j] = sum;
        }
    }
    return result;
}

function downloadMatricesFile() {
    console.log("downloading");

    const fileContent = "Movement Matrices:\n" + movementMatrices.map(matrix => matrix.map(row => row.join(', ')).join('\n')).join('\n\n') +
        "\n\nScaling Matrices:\n" + scaleMatrices.map(matrix => matrix.map(row => row.join(', ')).join('\n')).join('\n\n')+
        "\n\nTransformed Points Matrices:\n" + transformedPointsMatrices.map(matrix => matrix.map(row => row.join(', ')).join('\n')).join('\n\n');
    const blob = new Blob([fileContent], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'matrices.txt';
    link.click();
    URL.revokeObjectURL(url);
}