const canvas = document.querySelector("#canv");
const ctx = canvas.getContext("2d");
let canvasWidth = 1000;
let canvasHeight = 800;
let canvasGridsize = 25;

const methodP = document.querySelector("#method");
let method = 0;

const pointsList = [];

class Point{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }

    drawPoint(){
        const canvasX = canvasWidth/2 + this.x * canvasGridsize;
        const canvasY = canvasHeight/2 - this.y * canvasGridsize;

        ctx.beginPath();
        ctx.arc(canvasX, canvasY, 5, 0, Math.PI * 2);
        ctx.fillStyle = 'red';
        ctx.fill();
    }
}

document.querySelector("#button-resolution-save").addEventListener('click',function(){
    canvasGridsize = +document.querySelector("#input-cnv-gridsize").value;

    console.log(canvasWidth,canvasHeight,canvasGridsize);

    createGrid();
});

document.querySelector("#button-clear").addEventListener('click',function(){
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    pointsList.splice(0, pointsList.length);
    document.querySelector("#points").innerHTML = "";
});

document.querySelector("#button-method-param").addEventListener('click',function(){
    methodP.innerHTML = "parametrical";
    method = 0;
});

document.querySelector("#button-method-matrix").addEventListener('click',function(){
    methodP.innerHTML = "matrix";
    method = 1;
});

document.querySelector("#button-draw-bezier").addEventListener('click', function(){
    if(pointsList.length == 0 || pointsList.length == 1){
        alert("add more points");
        return;
    }
    else{
        drawBezier();
    }
});

document.querySelector("#button-clear-points").addEventListener('click', function(){
    createGrid();
    pointsList.splice(0, pointsList.length);
    document.querySelector("#points").innerHTML = "";
});

document.querySelector("#curve-color").addEventListener('input', function() {
    stroke = this.value;
});

document.querySelector("#poly-color").addEventListener('input', function() {
    strokePoly = this.value;
});

document.querySelector("#button-add").addEventListener('click', function (){
    let x = +document.querySelector("#input-x").value;
    let y = +document.querySelector("#input-y").value;

    let point = new Point(x,y);
    
    point.drawPoint();

    pointsList.push(point);
    
    let pointsSpan = "";
    for(let i = 0; i < pointsList.length; i++) {
        if (i === 0 || i === pointsList.length - 1) {
            pointsSpan += ` [${i+1}]{<span style="color:red;">${pointsList[i].x} ; ${pointsList[i].y}</span>} `;
        } else {
            pointsSpan += ` [${i+1}]{${pointsList[i].x} ; ${pointsList[i].y}} `;
        }
    }
    
    document.querySelector("#points").innerHTML = pointsSpan;
})

document.querySelector("#button-download").addEventListener('click', function(){
    var content = '';
    content += `Curve order = ${pointsList.length-1}\n`

    for (var i = 0; i < M.length; i++) {
        for (var j = 0; j < M[i].length; j++) {
            if (M[i][j] !== 0) {
                content += `(${i}, ${j}): ${M[i][j]}\n`;
            }
        }
    }

    var blob = new Blob([content], { type: 'text/plain' });

    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = 'non_zero_elements.txt';

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
})

document.querySelector("#button-calculate").addEventListener('click',function(){

    let t1 = +document.querySelector('#input-t1').value;
    let t2 = +document.querySelector('#input-t2').value;
    let step = +document.querySelector('#input-step').value;

    if(t1 > t2 || t1<0 || t2 > 1){
        alert("Wrong t");
        return;
    }

    if (pointsList.length < 2) {
        alert("Add more points to define the curve.");
        return;
    }

    let content = '';

    for (let t = t1; t <= t2; t += step) {
        let x = calculateBezier(t, pointsList.map(point => point.x));
        let y = calculateBezier(t, pointsList.map(point => point.y));
        content+=`t = ${t.toFixed(2)}, x = ${x.toFixed(2)}, y = ${y.toFixed(2)}\n`
        console.log(`t = ${t.toFixed(2)}, x = ${x.toFixed(2)}, y = ${y.toFixed(2)}`);
    }
    alert(content);
});

function pointsSpanUpdate() {
    let pointsSpan = "";
    for(let i = 0; i < pointsList.length; i++) {
        if (i === 0 || i === pointsList.length - 1) {
            pointsSpan += ` [${i+1}]{<span style="color:red;">${parseFloat(pointsList[i].x).toPrecision(3)} ; ${parseFloat(pointsList[i].y).toPrecision(3)}</span>} `;
        } else {
            pointsSpan += ` [${i+1}]{${parseFloat(pointsList[i].x).toPrecision(3)} ; ${parseFloat(pointsList[i].y).toPrecision(3)}}`;
        }
    }
    
    document.querySelector("#points").innerHTML = pointsSpan;
}

function createGrid()
{
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;

    const gridColor = 'grey';
    const axisColor = 'black';

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // Draw vertical grid lines
    for (let x = canvasGridsize; x < canvas.width; x += canvasGridsize) {
        ctx.beginPath();
        ctx.moveTo(x, 0);
        ctx.lineTo(x, canvas.height);
        ctx.strokeStyle = gridColor;
        ctx.stroke();
    }

    // Draw horizontal grid lines
    for (let y = canvasGridsize; y < canvas.height; y += canvasGridsize) {
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(canvas.width, y);
        ctx.strokeStyle = gridColor;
        ctx.stroke();
    }

    ctx.strokeStyle = axisColor;
    ctx.lineWidth = 2;

    ctx.beginPath();
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width / 2, canvas.height);
    ctx.stroke();

    ctx.fillStyle = axisColor;
    ctx.font = "12px Arial";
    ctx.textAlign = "center";
    
    for (let x = canvasGridsize; x < canvas.width; x += canvasGridsize) {
        if(x === canvas.width/2){
            continue;
        }
        ctx.fillText((x - canvas.width / 2) / canvasGridsize, x, canvas.height / 2 + 15);
    }
    ctx.fillText("X", canvas.width - 10, canvas.height / 2 + 15);

    for (let y = canvasGridsize; y < canvas.height; y += canvasGridsize) {
        if(y === canvas.height/2){
            continue;
        }
        ctx.fillText(-(y - canvas.height / 2) / canvasGridsize, canvas.width / 2 - 10, y + 5);
    }
    ctx.fillText("Y", canvas.width / 2 - 10, 10);

    drawArrowhead(ctx, canvas.width, canvas.height / 2, Math.PI / 2); 
    drawArrowhead(ctx, canvas.width / 2, 0, 0);
}

function drawArrowhead(ctx, x, y, angle) {
    ctx.save();
    ctx.beginPath();
    ctx.translate(x, y);
    ctx.rotate(angle);
    ctx.moveTo(0, 0);
    ctx.lineTo(5, 10);
    ctx.lineTo(-5, 10);
    ctx.closePath();
    ctx.fill();
    ctx.restore();
}

function drawPoints() {
    pointsList.forEach(p => {
        p.drawPoint();
    })
}

function calculateBernstein(t, n, k){
    return (factorial(n) / (factorial(k) * factorial(n - k)) * Math.pow(t, k) * Math.pow((1 - t), (n - k)));
}

function calculateBezier(t, arrayPoints){
    arrayPointsLength = arrayPoints.length;
    let returnValue = 0;
    for(let i = 0; i < arrayPointsLength; i++){
        returnValue += calculateBernstein(t, arrayPointsLength - 1, i) * arrayPoints[i];
    }
    return returnValue;
}

let stroke = "black";
let strokePoly = "black";
let strokeWidth = 2;

function drawBezier() {
    createGrid();
    drawPoints();
    drawPoly();
    if(method == 0) {
        console.log("PARAMETRICAL");
        let step = 1 / parseFloat(1000);
        ctx.strokeStyle = stroke;
        ctx.lineWidth = 2
        ctx.beginPath();
        let x = calculateBezier(0, pointsList.map(point => point.x));
        let y = calculateBezier(0, pointsList.map(point => point.y));
        ctx.moveTo(x * canvasGridsize + canvasWidth / 2, canvasHeight / 2 - y * canvasGridsize);
        for(let i = step; i < 1; i += step){
            x = calculateBezier(i, pointsList.map(point => point.x));
            y = calculateBezier(i, pointsList.map(point => point.y));
            ctx.lineTo(x * canvasGridsize + canvasWidth / 2, canvasHeight / 2 - y * canvasGridsize);
        }
        x = calculateBezier(1, pointsList.map(point => point.x));
        y = calculateBezier(1, pointsList.map(point => point.y));
        ctx.lineTo(x * canvasGridsize + canvasWidth / 2, canvasHeight / 2 - y * canvasGridsize);
        ctx.stroke();
        ctx.closePath();
    }
    else if(method == 1) {
        console.log("MATRIX");
        
        const n = pointsList.length;
        let M = bezierMatrix(n);

        ctx.lineWidth = 2;
        ctx.strokeStyle = stroke;
        ctx.beginPath();
        ctx.moveTo(pointsList[0].x * canvasGridsize + canvasWidth / 2, canvasHeight / 2 - pointsList[0].y * canvasGridsize);
      
        console.log(M);
        for (let t = 0; t <= 1; t += 0.001) {
          let T = [];
          for (let k = n - 1; k >= 0; k--) {
            T.push(t ** k);
          }
      
          let p = new Point(0, 0);
          for (let i = 0; i < T.length; i++) {
            const factor = T.reduce((acc, val, index) => acc + val * M[i][index], 0);
            p.x += (pointsList[i].x * canvasGridsize +canvasWidth/2) * factor;
            p.y += (canvasHeight/2-pointsList[i].y*canvasGridsize) * factor;
          }
          ctx.lineTo(p.x, p.y);
        }
        ctx.stroke();
        ctx.closePath();
    }
    else {
        alert("No method selected");
    }
}

function drawPoly(){
    ctx.beginPath();
    ctx.strokeStyle = strokePoly;
    ctx.lineWidth = 2;
    ctx.moveTo(pointsList[0].x*canvasGridsize+canvasWidth/2,canvasHeight/2-pointsList[0].y*canvasGridsize);
    
    for(let i = 1; i < pointsList.length; i++)
    {
        ctx.lineTo(pointsList[i].x*canvasGridsize+canvasWidth/2,canvasHeight/2-pointsList[i].y*canvasGridsize)
    }
    ctx.stroke();
    ctx.closePath();
}

function nCi(n, k) {
    if (k < 0 || k > n) {
        return 0;
    }
    if (k === 0 || k === n) {
        return 1;
    }
    
    let result = 1;

    if (k > n - k) {
        k = n - k;
    }

    for (let i = 0; i < k; i++) {
        result *= (n - i);
        result /= (i + 1);
    }

    return result;
}

let M;
function bezierMatrix(n) {
    M = new Array(n).fill(null).map(() => new Array(n).fill(null));
    
    for (let i = 0; i < n; i++) {
      for (let j = 0; j < n; j++) {
        if (i <= j) {
          M[i][j] = Math.pow(-1, j - i) * nCi(n - 1, i) * nCi(n - i - 1, j - i);
        } else {
          M[i][j] = 0;
        }
      }
    }
  
    for (let i = 0; i < n; i++) {
      M[i].reverse();
    }
  
    return M;
}

function factorial(i){
    if(i == 1 || i == 0){
        return 1;
    }
    else{
        return i * factorial(i - 1);
    }
}

let draggingPoint = false;
let draggedPointIndex = -1;
let distance = 10e8;

canvas.addEventListener('mousedown', function(event) {
    const rect = canvas.getBoundingClientRect();
    const mouseX = event.clientX - rect.left;
    const mouseY = event.clientY - rect.top;

    if (event.button === 0) { 
        for (let i = 0; i < pointsList.length; i++) {
            const point = pointsList[i];
            const canvasX = canvasWidth / 2 + point.x * canvasGridsize;
            const canvasY = canvasHeight / 2 - point.y * canvasGridsize;
            distance = Math.sqrt((mouseX - canvasX) ** 2 + (mouseY - canvasY) ** 2);
            if (distance <= 5) { 
                draggingPoint = true;
                draggedPointIndex = i;
                break;
            }
        }
    }
});

canvas.addEventListener('mousemove', function(event) {
    if (draggingPoint) {
        const rect = canvas.getBoundingClientRect();
        const mouseX = event.clientX - rect.left;
        const mouseY = event.clientY - rect.top;

        const x = (mouseX - canvasWidth / 2) / canvasGridsize;
        const y = (canvasHeight / 2 - mouseY) / canvasGridsize;
        pointsList[draggedPointIndex].x = x;
        pointsList[draggedPointIndex].y = y;

        createGrid();
        drawPoints();
        drawBezier(); 
        pointsSpanUpdate();
    }
});

canvas.addEventListener('click', function(event) {

    if(distance < 5) return;

    const rect = canvas.getBoundingClientRect();
    const mouseX = event.clientX - rect.left;
    const mouseY = event.clientY - rect.top;

    const canvasX = (mouseX - canvas.width / 2) / canvasGridsize;
    const canvasY = (canvas.height / 2 - mouseY) / canvasGridsize;

    const point = new Point(canvasX, canvasY);
    point.drawPoint();
    pointsList.push(point);
    pointsSpanUpdate();

    if (pointsList.length > 1) drawBezier();
});

canvas.addEventListener('mousedown', function(event) {
    const rect = canvas.getBoundingClientRect();
    const mouseX = event.clientX - rect.left;
    const mouseY = event.clientY - rect.top;
    const isRightClick = event.button === 2;

    if (isRightClick) {

        let pointIndexToDelete = -1;
        for (let i = 0; i < pointsList.length; i++) {
            const point = pointsList[i];
            const canvasX = canvasWidth / 2 + point.x * canvasGridsize;
            const canvasY = canvasHeight / 2 - point.y * canvasGridsize;
            const distance = Math.sqrt((mouseX - canvasX) ** 2 + (mouseY - canvasY) ** 2);
            if (distance <= 5) {
                pointIndexToDelete = i;
                break;
            }
        }
        if (pointIndexToDelete !== -1) {
            pointsList.splice(pointIndexToDelete, 1);
            createGrid();
            drawPoints();
            drawBezier(); 
            pointsSpanUpdate();
        }
    }
});


canvas.addEventListener('mouseup', function(event) {
    draggingPoint = false;
    draggedPointIndex = -1;
});

canvas.addEventListener('mouseleave', function(event) {
    draggingPoint = false;
    draggedPointIndex = -1;
});

canvas.addEventListener('contextmenu', function(event) {
    event.preventDefault();
});

