const canvas = document.getElementById('canv');
const ctx = canvas.getContext('2d');

trapezoids = []

_gridHeight = 0
_gridWidth = 0
_gridSize = 0

function buttonSaveSize()
{
    let w = Number(document.getElementById("fieldGridWidth").value);
    let h = Number(document.getElementById("fieldGridHeight").value);
    let s = Number(document.getElementById("fieldGridSize").value);

    console.log("width = " + w);
    console.log("height = " + h);
    console.log("grid size = " + s);

    createGrid(w,h,s);
}

function buttonCreateTrapezoid()
{
    let tl = new Point(document.getElementById("field1").value);
    let bl = new Point(document.getElementById("field2").value);
    let tr = new Point(document.getElementById("field3").value);
    let br = new Point(document.getElementById("field4").value);

    console.log("tl point: x = " + tl.x + " y = " + tl.y);
    console.log("bl point: x = " + bl.x + " y = " + bl.y);
    console.log("tr point: x = " + tr.x + " y = " + tr.y);
    console.log("br point: x = " + br.x + " y = " + br.y);

    const pointsArray = [tl,tr,br,bl];
    console.log(pointsArray);

    trap = new Trapezoid(pointsArray);

    trapezoids.push(trap);
    console.log(trapezoids);

    let colorChosen = document.getElementById("checkUseColor");

    if(colorChosen.checked) {
        color = document.getElementById("colorPicker").value;
    }
    else {
        color = null; 
    }

    DrawTrapezoid(trap, color);
}

function createGrid(w, h, s) {
    canvas.width = w;
    canvas.height = h;

    const gridSize = s;
    const gridColor = '#ddd';
    const axisColor = '#76767c';

    _gridHeight = h;
    _gridWidth = w;
    _gridSize = s;

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.strokeStyle = gridColor;
    ctx.lineWidth = 1;

    // Draw vertical grid lines
    for (let x = gridSize; x < canvas.width; x += gridSize) {
        ctx.beginPath();
        ctx.moveTo(x, 0);
        ctx.lineTo(x, canvas.height);
        ctx.stroke();
    }

    // Draw horizontal grid lines
    for (let y = gridSize; y < canvas.height; y += gridSize) {
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(canvas.width, y);
        ctx.stroke();
    }

    ctx.strokeStyle = axisColor;
    ctx.lineWidth = 2;

    ctx.beginPath();
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width / 2, canvas.height);
    ctx.stroke();

    ctx.fillStyle = axisColor;
    ctx.font = "12px Arial";
    ctx.textAlign = "center";
    
    for (let x = gridSize; x < canvas.width; x += gridSize) {
        if(x === canvas.width/2){
            continue;
        }
        ctx.fillText((x - canvas.width / 2) / gridSize, x, canvas.height / 2 + 15);
    }
    ctx.fillText("X", canvas.width - 10, canvas.height / 2 + 15);

    for (let y = gridSize; y < canvas.height; y += gridSize) {
        if(y === canvas.height/2){
            continue;
        }
        ctx.fillText(-(y - canvas.height / 2) / gridSize, canvas.width / 2 - 10, y + 5);
    }
    ctx.fillText("Y", canvas.width / 2 - 10, 10);

    drawArrowhead(ctx, canvas.width, canvas.height / 2, Math.PI / 2); 
    drawArrowhead(ctx, canvas.width / 2, 0, 0);
}

function drawArrowhead(ctx, x, y, angle) {
    ctx.save();
    ctx.beginPath();
    ctx.translate(x, y);
    ctx.rotate(angle);
    ctx.moveTo(0, 0);
    ctx.lineTo(5, 10);
    ctx.lineTo(-5, 10);
    ctx.closePath();
    ctx.fill();
    ctx.restore();
}

class Point{
    constructor(x, y) {
        if (typeof x === 'string') {
            const [xCoord, yCoord] = x.split(' ').map(parseFloat);
            this.x = xCoord || 0;
            this.y = yCoord || 0;
        } else {
            this.x = x || 0;
            this.y = y || 0;
        }
    }
}

class Trapezoid{
    constructor(points = [])
    {
        if(points.length != 4){
            throw new Error("Trapezoid constructor requires exactly 4 points.");
        }
        if (!this.isTrapezoid(points)) {
            alert("The provided points do not form a trapezoid.");
            throw new Error("The provided points do not form a trapezoid.");
        }
        this.points = points;
    }

    isTrapezoid(points) {
        if (points.length !== 4) {
            return false;
        }
        
        const side1 = this.calculateDistance(points[0], points[1]);
        const side2 = this.calculateDistance(points[1], points[2]);
        const side3 = this.calculateDistance(points[2], points[3]);
        const side4 = this.calculateDistance(points[3], points[0]);

        const isParallel = this.areParallel(points);

        const isUnequalSides = side1 !== side3 || side2 !== side4;

        return isParallel && isUnequalSides;
    }

    calculateDistance(point1, point2) {
        return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
    }

    areParallel(points) {
        const slope1 = this.calculateSlope(points[0], points[1]);
        const slope2 = this.calculateSlope(points[2], points[3]);

        return slope1 === slope2;
    }

    calculateSlope(point1, point2) {
        return (point2.y - point1.y) / (point2.x - point1.x);
    }

    calculateMidpoints() {
        const mid1 = new Point((this.points[0].x + this.points[1].x) / 2,
                             (this.points[0].y + this.points[1].y) / 2);
        
        const mid2 = new Point((this.points[2].x + this.points[3].x) / 2,
                               (this.points[2].y + this.points[3].y) / 2);
        
        const mid3 = new Point((this.points[1].x + this.points[2].x) / 2,
                               (this.points[1].y + this.points[2].y) / 2);
        
        const mid4 = new Point((this.points[3].x + this.points[0].x) / 2,
                               (this.points[3].y + this.points[0].y) / 2);
        

        return [mid1, mid2, mid3, mid4];
    }
}

function DrawTrapezoid(trap, color)
{
    if(trap instanceof Trapezoid){
        ctx.beginPath();
        ctx.moveTo(trap.points[0].x * _gridSize + _gridWidth / 2, _gridHeight / 2 - trap.points[0].y * _gridSize);
        for(let i = 1; i < trap.points.length; i++) {
            ctx.lineTo(trap.points[i].x * _gridSize + _gridWidth / 2, _gridHeight / 2 - trap.points[i].y * _gridSize);
        }

        if(color != null) {
            ctx.fillStyle = color;
            ctx.fill();
        }

        ctx.closePath();
        ctx.stroke();


        let mediansChosen = document.getElementById("checkMedians");

        if(mediansChosen.checked) {
            const midpoints = trap.calculateMidpoints();
            for(let i = 0; i < trap.points.length; i++) {
                ctx.beginPath();

                for(let j = 0; j < midpoints.length; j++)
                {
                    ctx.moveTo(trap.points[i].x * _gridSize + _gridWidth / 2, _gridHeight / 2 - trap.points[i].y * _gridSize);
                    ctx.lineTo(midpoints[j].x * _gridSize + _gridWidth / 2, _gridHeight / 2 - midpoints[j].y * _gridSize);
                    ctx.stroke();
                }
                ctx.closePath();
            }
        }


    } else {
        console.error("Parameter of DrawTrapezoid was not an instance of Trapezoid class.")
    }
}



function buttonClearGrid()
{
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    buttonSaveSize();

    trapezoids = []
}